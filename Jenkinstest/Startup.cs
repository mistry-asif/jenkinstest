﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Jenkinstest.Startup))]
namespace Jenkinstest
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
